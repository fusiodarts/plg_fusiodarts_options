<?php
/**
 * ------------------------------------------------------------------------
 * fusiodarts_options.php - Fusiodarts Options (plugin)
 * ------------------------------------------------------------------------
 * @author       Angel Albiach - Fusió d'Arts Technology S.L.
 * @copyright    Copyright (c) 2016 Fusió d'Arts All rights reserved.
 * @license     GNU General Public License version 2 or later;
 * @website      http://www.fusiodarts.com
 *------------------------------------------------------------------------
*/

defined('JPATH_BASE') or die;
jimport('joomla.plugin.plugin');

class plgSystemFusiodarts_Options extends JPlugin
{

	public function onBeforeRender()
	{
		$this->disableFeed();
	}

	public function onAfterRender()
	{
		if ($this->params->get('minify_html', 0) == '1') {
			$this->minifyHtml();
		}
	}

	public function onAfterInitialise()
	{
		$this->disableClass();
	}

/*=============================================================
=  FUNCTIONS FUSIODARTS
==============================================================*/

	// Disable Joomla Class
	private function disableClass()
	{
		if (JFactory::getApplication()->isAdmin()) return;

		if ($this->params->get('disable_mootools', 0) == '1') include 'disableclass/behavior.php';

		if ($this->params->get('disable_jquery', 0) == '1') include 'disableclass/jquery.php';

		if ($this->params->get('disable_bootstrap', 0) == '1') include 'disableclass/bootstrap.php';

		if ($this->params->get('disable_feed', 0) == '1') include 'disableclass/feed.php';

		return;
	}

	// Eliminas espacios y lineas en blanco del html
	private function minifyHtml()
	{
		$app = JFactory::getApplication();

		if ($app->isAdmin())
		{
			return true;
		}

		$body = JResponse::getBody();

		$regex = '/(^[\t ]+)|([\t ]+?$)/im';
		$body = preg_replace($regex, '', $body);

		$regex = '/^\n+|^\r\n+/im';
		$body = preg_replace($regex, '', $body);

		JResponse::setBody($body);

		return;
	}

	// Eliminamos las etiquetas tipo feed del head
	private function disableFeed()
	{
		$doc = JFactory::getDocument();

		if ($this->params->get('disable_feed', 0) == '1')
		{

			$datahead = $doc->getHeadData();

			$headlinks = $datahead['links'];
			foreach ($headlinks as $key => $value) {
				if (strpos($key, 'format=feed') !== false) {
					unset($datahead['links'][$key]);
				}
			}
			$doc->setHeadData($datahead);
		}

		return;
	}

}